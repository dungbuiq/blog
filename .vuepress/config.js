module.exports = {
    title: 'SSD của tôi',
    description: 'Đây là nơi chia sẻ, lưu trữ lại các kiến thức kĩ thuật.',
    head: [
        ['link', { rel: 'icon', href: 'https://img.icons8.com/material/4ac144/256/user-male.png' }]
    ],
    themeConfig: {
        sidebar: 'auto',
        searchPlaceholder: 'Tìm kiếm...',
        lastUpdated: 'Last Updated',
        nav: [
            { text: 'trang chủ', link: '/' },
            { text: 'vể tôi', link: '/about/' },
            { text: 'Group1', items: [{ text: 'Ehkoo', link: 'https://ehkoo.com' }] },
        ],
    },
    dest: 'docs'
}